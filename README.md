# patient-classification
> Sean Nesdoly  
> June 15th, 2017  
> Duan Laboratory

## Purpose
This script classifies cancer patients as **resistant** or **sensitive** to platinum-based chemotherapy treatments. Resistance was defined as a threshold number of days between the end of the most recent drug therapy treatment and the onset of a new tumor or progression of an existing one. Identifying patients that are resistant to platinum treatment and those that are not provides critical information that may help elucidate the genetic basis of cancer and further informs individual treatment plans.

## Definitions
-  A patient is **resistant** if the interval between their last platinum treatment and the onset of a recurrent tumor or progression of an existing one is `<= 182 days` (6 months).
- A patient is **sensitive** if the interval between their last platinum treatment and the onset of a recurrent tumor or progression of an existing one is `> 365 days`.
- The interval from the date of last primary platinum treatment to the date of progression, date of recurrence, or date of last known contact if the patient is alive and has not recurred is defined as the **platinum free interval (PFI)**
	- taken from the *Integrated genomic analyses of ovarian carcinoma* article by The Cancer Genome Atlas Research Network

## Data reduction & filtering
1. Query and download clinical data from the *Genomic Data Commons* TCGA-OV project
	- data sets: `patient`, `new_tumor_event`, `drug`, `follow_up`, `stage_event`
2. Merge `new_tumor_event` & `drug` to match patient tumor events with drug therapy treatments
3. Compute `diff` variable for each entry:  
	- `days_to_new_tumor_event_after_initial_treatment` - `days_to_drug_therapy_end`  
4. *Select* only platinum-based drug therapy treatments
5. *Remove* entries where `diff`==*NA* (unable to classify)
6. *For each patient*, select the last treatment before tumor recurrence/progression
	- remove treatments that have start/end dates occurring after the recurrent/progressed tumor date
7. Add `clinical_stage` variable for each patient
8. Collect patients that are **tumor free** and **not** in the `new_tumor_event` dataset
	- TODO: exclude patients with days_to_last_follow_up < 182 
9. *Classify* patients
	- `resistant_relapse`: diff <= 182
	- `middle`: 182 < diff <= 365
	- `sensitive_relapse`: diff > 365
	- `sensitive_no_relapse`: no recurrence/progression && days_to_last_follow_up > 182
		- `excluded`: no recurrence/progression && days_to_last_follow_up < 182
10. *Assert* that the resistant and sensitive patient barcode sets are disjoint
11. *Write* results to file
